parent(jhon, bob).
parent(lili, bob).

female(lili).
male(jhon).

% conjuction
father(X, Y) :- parent(X, Y), male(X).
mother(X, Y) :- parent(X, Y), female(X).

% disjunction
child_of(X, Y) :- mother(Y, X); father(Y, X).